var PAGE_1_ANIMATION_SPEED = 600;

var page_1 = function(){

	this.init = function() {
		$("html, body, #sections").css("overflow-y", "hidden");

		section_height = $("body").height() - $("#page_1 .section_body").offset().top

		$("#page_1 .bg").css("height", section_height);
		

		$("#page_1").find(".section_content").fadeIn();
		
		$("#page_1 .bg").css("height", $("#page_1").height());
		$("#page_1 .section_body").css("height",  section_height);
		
		$("#div1").stop().animate({top: 0, 
				opacity: 1
			}, 
			PAGE_1_ANIMATION_SPEED, 
			"easeInOutExpo"
		);
		
		$("#div2").stop().delay(PAGE_1_ANIMATION_SPEED * 1.2).animate({top: 0, 
				opacity: 1
			}, 
			PAGE_1_ANIMATION_SPEED, 
			"easeInOutExpo"
		);

		$("#div3").stop().delay(PAGE_1_ANIMATION_SPEED * 1.3).animate({top: 0, 
				opacity: 1
			}, 
			PAGE_1_ANIMATION_SPEED, 
			"easeInOutExpo"
		);

		$("#div4").stop().delay(PAGE_1_ANIMATION_SPEED * 1.4).animate({top: 0, 
				opacity: 1
			}, 
			PAGE_1_ANIMATION_SPEED, 
			"easeInOutExpo",
			function() {

				//$("#page_1 .section_body").css("overflow", "auto");
				//$("#the_site").css("overflow", "auto");
				$("html, body, #sections").css("overflow-y", "auto");
				$("#page_1 .section_body").css("height",  "auto");
				$("#page_1 .bg").css("height", $("#page_1").height());
			}
		);

	}
};
