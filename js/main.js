
var TRANSITION_DELAY = 900;
var NEW_SECTION_TRANSITION_DELAY = 800;
var NEW_SECTION_OPACITY_START = 0.8;

function loadSite() {
    build_menu();
    hash = window.location.hash;
    route = hash.replace("#", "").replace("!", "");
    if (route != "" || route == "#") {

        $(".trigger_" + route).trigger("click");
    } else {
        $(".logo").trigger("click");
    }
    
    $("#counter").fadeOut("slow", function(){
        $("#the_site").fadeIn();
    });
}

function build_menu() {

    $.each(structure, function (i,el) {
        //console.log(el);
        var el_id = el.id;
        if (el.menu_title) {
            el_title = el.menu_title;
            new_item = $("<li></li>");
            new_item.html("<a class='section_link trigger_" + el.id + "' data-target='"+ el_id +"'>" + el_title +"</a>");
            $(new_item).appendTo($("#menu"));
        }
    });

    activate_menu();

}

function activate_menu() {

    $( '.section_link' ).click(load_section)
    console.log("menu activated");
};

function deactivate_menu() {
    $( '.section_link' ).unbind("click");
    console.log("menu deactivated");
}

function load_section(e) {

    var new_section_id = $(this).data('target');
    var current_section = $("#sections").children().first();

    if  (current_section.attr("id") == (new_section_id)) return;
    deactivate_menu()
    $.each(structure, function (i,el) {

        if (el.id == new_section_id) {
            new_section = $("<div id='"+ el.id +"' class='section'></div>");
            if (el.controller) {
                $(new_section).addClass("bootable");
            }
            direction = (el.load_direction) ? el.load_direction : "up";
            switch (direction) {
                case "static" : {
                    scroll_static(current_section, new_section, el);
                }
                break;
                case "up" : {
                    scroll_vertical_up(current_section, new_section, el);
                } break;
                 case "down" : {
                    scroll_vertical_down(current_section, new_section, el);
                } break;
                case "left": {
                    scroll_horizontal_left(current_section, new_section, el)
                }
                break;
                default:
                case "right": {
                    scroll_horizontal_right(current_section, new_section, el)
                    
                }
                break;
            }
            return true;
        }
    });
}




/*********************************************************************************************/
/*              PAGE CHANGE ANIMATIONS
/*********************************************************************************************/

function scroll_horizontal_right(current_section, new_section, el) {

    // creo l'ombra a sinistra e la aggiungo a #sections
    hshadow = $("<div class='v-shadow'></div>");
    $(hshadow).appendTo($("#sections"));
    $("html, body, #the_site").css("overflow", "hidden");

    $(new_section).css({
            "left": "50%",
            "z-index": "50"
        }).load(el.tpl, function() {

            // aggiungo la nuova sezione a sections
            $(new_section).appendTo($("#sections"));

            var new_section_bg = $(new_section).find(".bg");
            new_section_bg.css("opacity", NEW_SECTION_OPACITY_START); 

            //scrollo sections
             $("#sections").stop().animate({
                left: "-100%"
            },{
                duration: TRANSITION_DELAY,
                easing: 'easeInOutExpo',
                start : function() {
                    $(new_section).stop().animate({
                        "left": "100%"
                    }, NEW_SECTION_TRANSITION_DELAY, 'easeInOutExpo')
                },
                complete: function() {

                    $(hshadow).fadeOut('fast', function() {
                        current_section.remove();
                        $("#sections").css("left", "0");
                        $(new_section).css({
                            "left" : 0,
                             "top" : 0,
                             "opacity": "1",
                             "z-index" : "100"
                        });
                        new_section_bg.stop().animate({
                            "opacity": "1"
                           },
                        NEW_SECTION_TRANSITION_DELAY);
                        // rimuovo l'ombra dal body
                        $(hshadow).remove();

                        //riattivo il menu
                        activate_menu();
                        display_section(new_section);
                        $("html, body, #the_site").css("overflow", "auto");
                    });
                }
            }
        );

    });

}

function scroll_horizontal_left(current_section, new_section, el) {

    // creo l'ombra a sinistra e la aggiungo a #sections
    hshadow = $("<div class='v-shadow-l'></div>");
    $(hshadow).appendTo($("#sections"));
    $("html, body, #the_site").css("overflow", "hidden");
    
    $(new_section).css({
            "left": "-50%",
            "z-index": "50"
        }).load(el.tpl, function() {

            // aggiungo la nuova sezione a sections
            $(new_section).appendTo($("#sections"));

            var new_section_bg = $(new_section).find(".bg");
            new_section_bg.css("opacity",NEW_SECTION_OPACITY_START); 

            //scrollo sections
             $("#sections").stop().animate({
                left: "100%"
            },{
                duration: TRANSITION_DELAY * 1.2,
                easing: 'easeInOutExpo',
                start : function() {
                    $(new_section).stop().animate({
                        "left": "-100%"
                    }, NEW_SECTION_TRANSITION_DELAY, 'easeInOutExpo')
                    
                },
                complete: function() {
                    $(hshadow).fadeOut('fast', function() {
                        current_section.remove();
                        $("#sections").css("left", "0");
                        $(new_section).css({
                            "left" : 0,
                             "top" : 0,
                             "opacity": "1",
                             "z-index" : "100"
                        });
                        new_section_bg.stop().animate({
                            "opacity": "1"
                           },
                        NEW_SECTION_TRANSITION_DELAY);
                        // rimuovo l'ombra dal body
                        $(hshadow).remove();

                        //riattivo il menu
                        activate_menu();
                        display_section(new_section);
                        $("html, body, #the_site").css("overflow", "auto");
                    });
                }
            }
        );

    });

}


function scroll_vertical_up(current_section, new_section, el) {

    hshadow = $("<div class='h-shadow'></div>");
    $(hshadow).appendTo($("#sections"));
    $("html").css("overflow", "hidden");

    $(new_section).css({
            "top": "50%",
            "z-index": "50",
           // "opacity": "0.9"
        }).load(el.tpl, function() {
            
            $(new_section).appendTo($("#sections"));

            var new_section_bg = $(new_section).find(".bg");
            new_section_bg.css("opacity", NEW_SECTION_OPACITY_START); 

            //scrollo sections
             $("#sections").stop().animate({
                top: "-100%"
            },{
                duration: TRANSITION_DELAY,
                easing: 'easeInOutExpo',
                start : function() {
                    $(new_section).stop().animate({
                        "top": "100%"
                    }, NEW_SECTION_TRANSITION_DELAY, 'easeInOutExpo')
                },
                complete: function() {

                    $(hshadow).fadeOut('fast', function() {
                        current_section.remove();
                        $("#sections").css("top", "0");
                        $(new_section).css({
                            "left" : 0,
                             "top" : 0,
                             "opacity": "1",
                             "z-index" : "100"
                        });
                        new_section_bg.stop().animate({
                            "opacity": "1"
                           },
                        NEW_SECTION_TRANSITION_DELAY);
                        // rimuovo l'ombra dal body
                        $(hshadow).remove();

                        //riattivo il menu
                        activate_menu();
                        display_section(new_section);
                        $("html").css("overflow", "auto");
                    });
                }
            }
        );
    });
}


function scroll_vertical_down(current_section, new_section, el) {

    hshadow = $("<div class='h-shadow-t'></div>");

    $(hshadow).appendTo($("#sections"));
    $("html").css("overflow", "hidden");

    $(new_section).css({
            "top": "-50%",
            "z-index": "50",
           // "opacity": "0.9"
        }).load(el.tpl, function() {
            
            $(new_section).appendTo($("#sections"));

            var new_section_bg = $(new_section).find(".bg");
            new_section_bg.css("opacity", NEW_SECTION_OPACITY_START); 

            //scrollo sections
             $("#sections").stop().animate({
                top: "100%"
            },{
                duration: TRANSITION_DELAY,
                easing: 'easeInOutExpo',
                start : function() {
                   $(new_section).stop().animate({
                        "top": "-100%"
                    }, NEW_SECTION_TRANSITION_DELAY, 'easeInOutExpo')
                },
                complete: function() {

                    $(hshadow).fadeOut('fast', function() {
                        current_section.remove();
                        $("#sections").css("top", "0");
                        $(new_section).css({
                            "left" : 0,
                             "top" : 0,
                             "opacity": "1",
                             "z-index" : "100"
                        });
                        new_section_bg.stop().animate({
                            "opacity": "1"
                           },
                        NEW_SECTION_TRANSITION_DELAY);
                        // rimuovo l'ombra dal body
                        $(hshadow).remove();

                        //riattivo il menu
                        activate_menu();
                        display_section(new_section);
                        $("html").css("overflow", "auto");
                    });
                }
            }
        );
    });
}

function scroll_static(current_section, new_section, el){
    $(new_section).load(el.tpl, function() {
        $(new_section).appendTo($("#sections"));
        current_section.remove();
        display_section(new_section);
        activate_menu()
    });
}


function display_section(section) {
    var section_id = $(section).attr("id");
    if (section_id != "home_page") {
        window.location.hash = "#!" + section_id;
    } else {
        window.location.hash = "";
    }
    if ($(section).hasClass("bootable")) {
         boot = new window[section_id]();
         boot.init();
    } else {
        $(section).find(".section_content").fadeIn();
    }
 }