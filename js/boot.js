var max_res_w = 1920;
var min_res_w = 1024;
var min_res_h = 768;


var assets = [
    "js/libs/jquery-1.10.1.min.js",
    "js/libs/jquery-ui.js",
    "js/libs/modernizr.custom.06548.js",
    "js/utils.js",
    "js/main.js",
    "css/structure.css",
    "css/template.css"
]

var onQueueProgress = function () {
    document.getElementById('counter').innerHTML = Math.floor(queue.progress * 100) + '%';
}

var onQueueLoaded = function () {
    loadSite()
}

var handleFileLoad = function (e) {
    if (e.item.type === 'javascript') {
        //console.log(e.item.src);
        document.getElementsByTagName('body')[0].appendChild(e.result);
    }
    else if (e.item.type === 'css') {
        document.getElementsByTagName('head')[0].appendChild(e.result);
    }
}


var queue = new createjs.LoadQueue(true);

queue.setMaxConnections(1);
queue.addEventListener("fileload", handleFileLoad);
queue.addEventListener("complete", onQueueLoaded);
queue.addEventListener("progress", onQueueProgress);

//push structure into assets
for (var n = 0; n < structure.length; n++) {
    el = structure[n];
    if (el.img != null) {
        assets.push(el.img);
    }
    if(el.css) {
        assets.push(el.css);
    }
    if (el.controller) {
        assets.push(el.controller);
    }
}

//load assets 
for (var a = 0; a < assets.length; a++) {
    console.log(assets[a]);
    queue.loadFile( assets[a], false);
}

queue.load();


