<?php

$sender = $_POST["email"];
$first_name = $_POST["nome"];
$last_name = $_POST["cognome"];
$message = strip_tags($_POST["messaggio"]);

$mail_body =<<<MSG

New Mail for Maglight from $first_name $last_name ($sender)

Message:

$message;
MSG;

$to      = 'dotlightusa@gmail.com';
//$to      = 'daniele.murroni@gmail.com';
$subject = 'New Mail from Maglight website';
$headers = "From: $sender \r\nReply-To: $sender\r\nX-Mailer: PHP/" . phpversion();

@mail($to, $subject, $mail_body, $headers);